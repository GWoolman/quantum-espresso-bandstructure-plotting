# Quantum Espresso Bandstructure Plotting

This program is based on a program originally written by Levi Lentz for the Kolpak Group at MIT This is distributed under the MIT license. 
Then edited by Gavin Woolman from the University of Edinburgh

This notebook is desined as a template for making bandstructure plots from quantum espresso output.

This template assumes you are using the template files of quartz.pp_bands.in

Which writes to quartz.pp_bands.out

Change filenames as needed
