#!/usr/bin/env python
# coding: utf-8

# # README
# 
# This was originally written by Levi Lentz for the Kolpak Group at MIT
# This is distributed under the MIT license
# Then edited by Gavin Woolman for University of Edinburgh
# 
# This notebook is desined as a template for making bandstructure plots from quantum espresso output. 
# 
# This template assumes you are using the template files of quartz.pp_bands.in
# 
# Which writes to quartz.pp_bands.out
# 
# Change filenames as needed
# 
# 
# 

# ## Functions

# In[71]:


import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import sys


# This function extracts the high-symmetry points from the file.pp_bands.out

# In[77]:



#This function extracts the high symmetry points from the output of quartz.pp_bands.out
def Symmetries(fstring): 
    f = open(fstring,'r')
    x = np.zeros(0)
    for i in f:
        if "high-symmetry" in i:
            x = np.append(x,float(i.split()[-1]))
    f.close()
    return x


# This program helps you put nice labels on the x-axis datapoints

# In[78]:


# Requires a subplot to plot to, 
# The symmetryfile which gives the positions of the special points along the x-axis
# a list, the same length as the number of special points, which gives the name of the special points in the Brillouin zone
def Plot_Special_Kpoints(subplot, symmetryfile, special_points_names):
    kpoint_locations = Symmetries(symmetryfile)
    if len(special_points_names) != len(kpoint_locations):
        raise Exception('Need the same number of names for your special kpoints as you have special kpoints')
    subplot.set_xticklabels(special_points_names)
    


# This function takes in the datafile quartz-bands.dat.gnu , the symmetry file, a subplot, and the label
# 
# It then plots the bandstructure to the subplot, along with the high-symmetry points

# In[82]:



# This function takes in the datafile, the fermi energy, the symmetry file, a subplot, and the label
# It then extracts the band data, and plots the bands, the fermi energy in red, and the high symmetry points
def bndplot(datafile,
            fermi,
            symmetryfile,
            subplot):
    z = np.loadtxt(datafile) #This loads the bandx.dat.gnu file
    x = np.unique(z[:,0]) #This is all the unique x-points
    bands = []
    bndl = len(z[z[:,0]==x[1]]) #This gives the number of bands in the calculation
    Fermi = float(fermi)
    axis = [min(x),max(x)]
    for i in range(0,bndl):
        bands.append(np.zeros([len(x),2])) #This is where we storre the bands
    for i in range(0,len(x)):
        sel = z[z[:,0] == x[i]]  #Here is the energies for a given x
        test = []
        for j in range(0,bndl): #This separates it out into a single band
            bands[j][i][0] = x[i]
            bands[j][i][1] = np.multiply(sel[j][1],13.605698066) # converts to electron volts. 
        
    for i in bands: #Here we plots the bands
        subplot.plot(i[:,0],i[:,1],color="black")
    
    temp = Symmetries(symmetryfile)
    
    subplot.set_xticks(temp)

    # FIND THE MIN and MAX ENERGIES
    emin=np.amin(bands[:][:])
    emax=np.amax(bands[:][:])
    
    for j in temp: #This is the high symmetry lines
        x1 = [j,j]
        x2 = [emin, emax]
        #x2 = [axis[2],axis[3]]
        subplot.plot(x1,x2,'--',lw=0.55,color='black',alpha=0.75)
    
    # Plot the Fermi Energy
    subplot.plot([min(x),max(x)],[Fermi,Fermi],color='red',linestyle='dashed')

        
    # Set the x-axis limits
    subplot.set_xlim([axis[0],axis[1]])
    


# ## Main Program
# 
# Here is a little program which gives an example plot

# In[83]:


fig, ax = plt.subplots(figsize=(8,8))

# Name of the output data file, as specified in quartz.pp_bands.in
# Value of Fermi energy (assume zero)
# Name  of the output of the post-processing bands step
# Subplot name
bndplot('bands/quartz-bands.dat.gnu', 
        0.0, 
        'bands/quartz.pp_bands.out', 
        ax)

ax.set_ylabel('Energy (eV)')

ax.set_ylim([-4,4])

# Edit the list of special k-point names
Plot_Special_Kpoints(ax, 
                     'bands/quartz.pp_bands.out', 
                     ['$\Gamma$', 'M', 'K', '$\Gamma$', 'A', 'L', 'H', '$\Gamma$'])

ax.set_title('Quartz Template Example')


# In[ ]:




